## Question 1

### What is the Feynman Technique? Paraphrase the video in your own words.


Feynman's learning technique states that if you can't explain a topic in simple terms then you don't understand the topic well enough. 

* If we identify a topic, take out a blank sheet of paper and write out everything as if we were teaching it to a child.

* Now remove the complex terms out of it as it must be simple.

* Review our notes to make sure that it is not so complicated.

* To test our understanding we can tun it by someone else.


---

## Question 2

### What are the different ways to implement this technique in your learning process?

* If we identify a topic, take out a blank sheet of paper and write out everything as if we were teaching it to a child.

* Now remove the complex terms out of it as it must be simple.

* Review our notes to make sure that it is not so complicated.

* To test our understanding we can tun it by someone else.

---

## Question 3

### Paraphrase the video in detail in your own words.

From the video we can come to know that how one can make the learning process more efficient.

* We should think of a solution for the problem with active focus but also take breaks in between so that your passive brain can think of new ways to solve the given issue.
* We must play with our known strengths and focus on what needs to be taken care of.

---

## Question 4

### What are some of the steps that you can take to improve your learning process?

* Approach a problem with active focus but also take breaks in between to think of passive solutions.
* Run the solution by someone else as it may open our thoughts and get to know something new.
* To know my strengths and weakness and work on improving them.

---
## Question 5

### Your key takeaways from the video? Paraphrase your understanding.

* To arrive solution with both active and passive thinking.
* To strengthen our weakness and areas where we need to focus on.
* To understand the problem in a simple term to deepen my understanding.

---

## Question 6

### What are some of the steps that you can while approaching a new topic?

* To understand my strengths in the given problem and try to improve the part where I am weak.

* Try to learn that in a way to teach anyone with simple terms.
