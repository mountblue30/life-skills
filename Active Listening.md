# Active Listening

## What is Active Listening?

The act of fully hearing and comprehending the meaning of what someone else is saying.

---

### 1) What are the steps/strategies to do Active Listening?

* Should avoid getting distracted by our own thoughts, focus on the speaker and topic instead.

* Should not interrupt the speaker, let them finish and respond after that.

* Should use phrases which are interactive and keep the speaker engaged.Some of the door openers are mentioned below.
    * Tell me more.
    * That sounds interesting.

* While engaged in a conversation we should use our body language to make the session more interactive and get the most out of it.

* We can take notes if it is an important session and that makes us more actively engaged in the conversation. And summarize the content to make sure that both are at the same page.

* Should be candid, open, and honest in our response to the speaker and treat the other person in a way that we think they would want to be treated.

---
### 2) According to Fisher's model, what are the key points of Reflective Listening?

* We must not be distracted by any means and should focus on the conversation leading to a successful information exchange.

* We should comply our genuine thoughts regarding the content spoken by the speaker instead of agreeing without acknowledging it.

* We should reflect the speaker's mood for the content delivered. If the speaker is happy about it we should also be happy while hearing it and not the contradiction.

* Should summarize the content and make sure that both are on same page by using the words of the speaker itself.

* During Reflective Listening the listner must be engaged more by being an observant and not pull the speaker into a chatter.

---

### 3) What are the obstacles in your listening process?

* The obstacles which block my listening process are:
    * Not being engaged completely.
    * Having thoughts of my own.
    * Interrupt the speaker before completion of the content.

---

### 4) What can you do to improve your listening?

* I can avoid being distracted by my own thoughts.
* Be more engaged in the conversation.
* Emotionally reflect the mood to be most out of it.
---

### 5) When do you switch to Passive communication style in your day to day life?

* When I have some works of my own and my mom wants something done at that moment, I will give priority for the work which my mom wants done.

* When I ask for a leave and it is not granted by my superior, I will comply with his saying.

* When my brother says something which I don't want to do.

---
### 6) When do you switch into Aggressive communication styles in your day to day life?

* When I have a valid point and still turned down for some other reasons.

* When someone accuses me for something which I didn't do.

---
### 7) When do you switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day to day life?

* When I am hurt and couldn't express my feelings completely.

* When there is chance to do a sarcastic comment with my friends.

---
### 8) How can you make your communication assertive?

* Can express my thoughts freely on something which is unwelcome my me.

* Make my valid point sound and clear, and state my intention so that it does not go misleading.